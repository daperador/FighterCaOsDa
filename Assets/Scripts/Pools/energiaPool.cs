using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class energiaPool : MonoBehaviour
{

    [SerializeField] private GameObject[] bolasEnergia;
    [SerializeField] private List<GameObject> bolasenergiaListAzul;
    [SerializeField] private List<GameObject> bolasenergiaListRoja;
    private int poolSize = 50;


    // Start is called before the first frame update
    void Start()
    {
        ItemsIntoPool(poolSize);
    }

    private void ItemsIntoPool(int poolSize) {

        for (int i = 0; i < poolSize; i++) {
            GameObject bolaRoja = Instantiate(bolasEnergia[0]);
            bolaRoja.SetActive(false);
            bolasenergiaListRoja.Add(bolaRoja);
            bolaRoja.transform.parent = transform;
        }


        for (int i = 0; i < poolSize; i++)
        {
            GameObject bolaAzul = Instantiate(bolasEnergia[1]);
            bolaAzul.SetActive(false);
            bolasenergiaListAzul.Add(bolaAzul);
            bolaAzul.transform.parent = transform;
        }


    }

    public GameObject InstanceBolaRoja() {
        for (int j = 0; j < bolasenergiaListRoja.Count; j++)
        {
            if (!bolasenergiaListRoja[j].activeSelf)
            {
                bolasenergiaListRoja[j].SetActive(true);
                return bolasenergiaListRoja[j];
            }
        }
        return null;
    }

    public GameObject InstanceBolaAzul(){
        for (int j = 0; j < bolasenergiaListAzul.Count; j++)
        {
            if (!bolasenergiaListAzul[j].activeSelf)
            {
                bolasenergiaListAzul[j].SetActive(true);
                return bolasenergiaListAzul[j];
            }
        }
        return null;
    }
}
