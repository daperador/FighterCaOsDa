using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameManager;

public class ItemsPool : MonoBehaviour
{
    [SerializeField] private GameObject [] itemsSpawnear;
    private int poolSize = 75;
    [SerializeField] private List<GameObject> itemsRojo;
    [SerializeField] private List<GameObject> itemsAzul;

    private bool pChange = false;
    public int nAle;
    private int divisionListSize;
    private static ItemsPool instance;
    public static ItemsPool Instance { get { return instance; } }
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        ItemsToPool(poolSize);
    }

    // Update is called once per frame
    void Update()
    {

    }


    private void ItemsToPool(int a)
    {

         divisionListSize = a / 3;

        for (int i = 0; i < a; i++)
        {
          
            if (i <= divisionListSize)
            {
                GameObject bolaRoja = Instantiate(itemsSpawnear[0]);
                bolaRoja.SetActive(false);
                itemsRojo.Add(bolaRoja);
                bolaRoja.transform.parent = transform;
            }
            else if (i > divisionListSize && i <= divisionListSize * 2)
            {
                GameObject pocionRoja = Instantiate(itemsSpawnear[1]);
                pocionRoja.SetActive(false);
                itemsRojo.Add(pocionRoja);
                pocionRoja.transform.parent = transform;
            }
            else {
                GameObject inmuneRojo = Instantiate(itemsSpawnear[2]);
                inmuneRojo.SetActive(false);
                itemsRojo.Add(inmuneRojo);
                inmuneRojo.transform.parent = transform;
            }
           
        }

        for (int i = 0; i < a; i++)
        {


            if (i < divisionListSize)
            {
                GameObject bolaAzul = Instantiate(itemsSpawnear[3]);
                bolaAzul.SetActive(false);
                itemsAzul.Add(bolaAzul);
                bolaAzul.transform.parent = transform;
            }
            else if (i > divisionListSize && i < divisionListSize * 2)
            {
                GameObject pocionAzul = Instantiate(itemsSpawnear[4]);
                pocionAzul.SetActive(false);
                itemsAzul.Add(pocionAzul);
                pocionAzul.transform.parent = transform;
            }
            else
            {
                GameObject inmuneAzul = Instantiate(itemsSpawnear[5]);
                inmuneAzul.SetActive(false);
                itemsAzul.Add(inmuneAzul);
                inmuneAzul.transform.parent = transform;
            }

        }


    }

   

    public GameObject InstanceItem()
    {
        nAle = Random.Range(0, 3);
        int posAlatoria = Random.Range(0, 75);

        if (nAle == 1) 
        {
            for (int j = 0; j < itemsRojo.Count; j++)
            {

                if (!itemsRojo[posAlatoria].activeSelf)
                {
                    itemsRojo[posAlatoria].SetActive(true);
                    return itemsRojo[posAlatoria];
                }

            }
            return null;
        }

        else
        {
            for (int j = 0; j < itemsAzul.Count; j++)
            {

                if (!itemsAzul[posAlatoria].activeSelf)
                {
                    itemsAzul[posAlatoria].SetActive(true);
                    return itemsAzul[posAlatoria];
                }

            }
            return null;
        }

    }
}
