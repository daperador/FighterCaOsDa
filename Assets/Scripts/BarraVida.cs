using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraVida : MonoBehaviour
{
    [SerializeField]
    private ScriptableInteger vida;

    [SerializeField]
    private Image barra;

    [SerializeField]
    private Color vidaVerde;
    [SerializeField]
    private Color vidaNaranja;
    [SerializeField]
    private Color vidaRoja;

    // Start is called before the first frame update
    void Start()
    {
        vida.valorActual = vida.valorTotal;
        ModificarVida();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ModificarVida()
    {
        barra.fillAmount = vida.valorActual / vida.valorTotal;
        if (barra.fillAmount <= 1f && barra.fillAmount >= .6f)
        {
            barra.color = vidaVerde;
        }
        else if (barra.fillAmount < .6f && barra.fillAmount >= .3f)
        {
            barra.color = vidaNaranja;
        }
        else { barra.color = vidaRoja; }

    }
}
