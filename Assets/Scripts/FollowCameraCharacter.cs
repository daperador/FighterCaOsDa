using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCameraCharacter : MonoBehaviour
{
    private Transform m_CharacterBlue;
    private Transform m_CharacterRed;
    public Transform CharacterBlue { get { return m_CharacterBlue; } set { m_CharacterBlue = value; } }
    public Transform CharacterRed { get { return m_CharacterRed; } set { m_CharacterRed = value; } }

    [Tooltip("Time it takes to reach the target object")]
    [SerializeField]
    private float m_FollowDelay = 1f;
    private Vector3 m_VectorSpeed = Vector3.zero;

    [SerializeField]
    private Vector3 m_Offset;


    // Update is called once per frame
    void Update()
    {
        if (CharacterBlue != null && CharacterRed != null)
        {
            float meitatTransformx = (CharacterBlue.transform.position.x + CharacterRed.transform.position.x) / 2;
            float meitatTransformy = (CharacterBlue.transform.position.y + CharacterRed.transform.position.y) / 2;

            transform.position = Vector3.SmoothDamp(transform.position, new Vector3(meitatTransformx, meitatTransformy+2, transform.position.z) + m_Offset, ref m_VectorSpeed, m_FollowDelay);
        }

    }
}
