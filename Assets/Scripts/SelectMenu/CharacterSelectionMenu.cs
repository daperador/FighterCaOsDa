﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterSelectionMenu : MonoBehaviour
{
    //Menu player 1
    public GameObject[] playerObjects;
    public int selectedCharacter = 0;

    private string selectedCharacterDataName = "SelectedCharacter";

    //Menu player 2
    public GameObject[] playerObjects2;
    public int selectedCharacter2 = 0;

    private string selectedCharacterDataName2 = "SelectedCharacter2";

    void Start()
    {

        HideAllCharacters();
        HideAllCharacters2();

        selectedCharacter = PlayerPrefs.GetInt(selectedCharacterDataName, 0);
        playerObjects[selectedCharacter].SetActive(true);

        selectedCharacter2 = PlayerPrefs.GetInt(selectedCharacterDataName2, 0);
        playerObjects2[selectedCharacter2].SetActive(true);
    }


    private void HideAllCharacters()
    {
        foreach (GameObject g in playerObjects)
        {
            g.SetActive(false);
        }
    }

    private void HideAllCharacters2()
    {
        foreach (GameObject a in playerObjects2)
        {
            a.SetActive(false);
        }
    }

    public void NextCharacter()
    {
        playerObjects[selectedCharacter].SetActive(false);
        selectedCharacter++;
        if (selectedCharacter >= playerObjects.Length)
        {
            selectedCharacter = 0;
        }
        playerObjects[selectedCharacter].SetActive(true);
    }

    public void PreviousCharacter()
    {
        playerObjects[selectedCharacter].SetActive(false);
        selectedCharacter--;
        if (selectedCharacter < 0)
        {
            selectedCharacter = playerObjects.Length-1;
        }
        playerObjects[selectedCharacter].SetActive(true);
    }

    public void NextCharacter2()
    {
        playerObjects2[selectedCharacter2].SetActive(false);
        selectedCharacter2++;
        if (selectedCharacter2 >= playerObjects2.Length)
        {
            selectedCharacter2 = 0;
        }
        playerObjects2[selectedCharacter2].SetActive(true);
    }

    public void PreviousCharacter2()
    {
        playerObjects2[selectedCharacter2].SetActive(false);
        selectedCharacter2--;
        if (selectedCharacter2 < 0)
        {
            selectedCharacter2 = playerObjects2.Length - 1;
        }
        playerObjects2[selectedCharacter2].SetActive(true);
    }

    public void StartGame()
    {
        if (selectedCharacter== selectedCharacter2)
        {
            print("You can't use the same character. Please, change it");
        }
        else 
        {
            PlayerPrefs.SetInt(selectedCharacterDataName, selectedCharacter);
            PlayerPrefs.SetInt(selectedCharacterDataName2, selectedCharacter2);
            SceneManager.LoadScene("SampleScene");
        }
        
    }

}
