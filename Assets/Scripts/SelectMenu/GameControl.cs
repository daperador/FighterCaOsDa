﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour
{
    //player 1
    public GameObject[] characters;
    public Transform playerStartPosition;
    private string selectedCharacterDataName = "SelectedCharacter";
    int selectedCharacter;
    public GameObject playerObject;
    //player2
    public GameObject[] characters2;
    public Transform playerStartPosition2;
    private string selectedCharacterDataName2 = "SelectedCharacter2";
    int selectedCharacter2;
    public GameObject playerObject2;

    [SerializeField] private energiaPool m_pool;
    [SerializeField] private Camera m_mainCamera;
    void Start()
    {
        selectedCharacter = PlayerPrefs.GetInt(selectedCharacterDataName,0);
         if (selectedCharacter == 1)
         {
             selectedCharacter = 0;
         }
         playerObject.GetComponent<CharacterController>().Pool = m_pool;
         playerObject = Instantiate(characters[selectedCharacter],playerStartPosition.position,characters[selectedCharacter].transform.rotation);
         m_mainCamera.GetComponent<FollowCameraCharacter>().CharacterBlue = playerObject.transform;

         selectedCharacter2 = PlayerPrefs.GetInt(selectedCharacterDataName2, 0);
         if (selectedCharacter2 == 0)
         {
             selectedCharacter2 = 1;
         }
         playerObject2.GetComponent<CharacterController>().Pool = m_pool;
         playerObject2 = Instantiate(characters2[selectedCharacter2], playerStartPosition2.position, characters2[selectedCharacter2].transform.rotation);
         m_mainCamera.GetComponent<FollowCameraCharacter>().CharacterRed= playerObject2.transform;

         playerObject.GetComponent<CharacterController>().jugadorContrari = playerObject2;
         playerObject2.GetComponent<CharacterController>().jugadorContrari = playerObject;

    }


}
