using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using UnityEngine.UIElements;

public class ShieldState : State
{
    string m_Animation;
    float m_StateLength;
    Coroutine m_Coroutine;
    CharacterController m_characterController;
    ScriptableCards m_cardScriptable;
    bool m_activeShield;
    public ShieldState(FSM fsm, string animation, float stateLength, ScriptableCards cardScriptable)
        : base(fsm)
    {
        m_Animation = animation;
        m_StateLength = stateLength;
        m_cardScriptable = cardScriptable;
        m_characterController = fsm.Owner.GetComponent<CharacterController>();
    }

    public override void Init()
    {
        if (m_cardScriptable.bolaInmunidad>0)
        {
            m_activeShield = true;
            m_Coroutine = m_FSM.Owner.GetComponent<CharacterController>().StartCoroutine(ChangeState(m_StateLength));
            m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
            m_characterController.Inmune = true;
            m_cardScriptable.bolaInmunidad--;
        }
        else
        {
            m_activeShield = false;
            m_FSM.ChangeState<IdleState>();
        }
        
    }

    public override void Exit()
    {
        base.Exit();
        if (m_activeShield)
        {
            m_FSM.Owner.GetComponent<CharacterController>().StopCoroutine(m_Coroutine);
        }
    }

    private IEnumerator ChangeState(float time)
    {
        yield return new WaitForSeconds(time);
        m_characterController.Inmune = false;
        m_FSM.ChangeState<IdleState>();
    }
}
