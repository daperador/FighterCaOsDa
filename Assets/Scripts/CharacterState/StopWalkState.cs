using UnityEngine;
using FiniteStateMachine;

public class StopWalkState : State
{
    float m_JumpForce;
    string m_movementAnimation;
    Rigidbody2D m_Rigidbody;
    CharacterController m_CharacterController;
    public StopWalkState(FSM fSM, string movementAnimation, float jumpForce)
        : base(fSM)
    {
        m_movementAnimation = movementAnimation;
        m_JumpForce = jumpForce;
        m_Rigidbody = fSM.Owner.GetComponent<Rigidbody2D>();
        m_CharacterController = fSM.Owner.GetComponent<CharacterController>();
    }

    public override void Init()
    {
        base.Init();
        m_Rigidbody.AddForce(Vector2.up * m_JumpForce, ForceMode2D.Impulse);
        m_FSM.Owner.GetComponent<Animation>().Play(m_movementAnimation);
        
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_Rigidbody.velocity.y < 0 && m_CharacterController.CheckGround().collider != null)
        {
            if (m_CharacterController.Actions.InFloor.Movement.ReadValue<float>() != 0)
                m_FSM.ChangeState<WalkState>();
            else
                m_FSM.ChangeState<IdleState>();
        }
    }
}

