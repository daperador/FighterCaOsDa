using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using UnityEngine.UIElements;

public class HitState : State
{
    string m_Animation;
    float m_StateLength;
    Coroutine m_Coroutine;
    CharacterController m_characterController;
    public HitState(FSM fsm, string animation, float stateLength)
        : base(fsm)
    {
        m_Animation = animation;
        m_StateLength = stateLength;
        m_characterController = fsm.Owner.GetComponent<CharacterController>();
    }

    public override void Init()
    {
        m_characterController.ImpulseInHit = 0.5f;
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        m_Coroutine = m_FSM.Owner.GetComponent<CharacterController>().StartCoroutine(ChangeState(m_StateLength));
        m_characterController.Damage = 3;
    }

    public override void Exit()
    {
        base.Exit();
        m_FSM.Owner.GetComponent<CharacterController>().StopCoroutine(m_Coroutine);
    }

    private IEnumerator ChangeState(float time)
    {
        yield return new WaitForSeconds(time);
        m_FSM.ChangeState<IdleState>();
    }
}
