using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using UnityEngine.InputSystem;
using System;

public class IdleState : State
{
    CharacterController m_characterController;
    string m_Animation;

    public IdleState(FSM fSM, string animation)
        : base(fSM)
    {
        m_characterController = fSM.Owner.GetComponent<CharacterController>();
        m_Animation = animation;
    }

    public override void Init()
    {
        base.Init();
        m_characterController.Actions.InFloor.Jump.performed += Jump;
        m_characterController.Actions.InFloor.Hit.performed += Hit;
        m_characterController.Actions.InFloor.Shield.performed += Shield;
        m_characterController.Actions.InFloor.Combo.performed += Combo;
        //m_characterController.Actions.InFloor.Movement.performed += Movement;
        //m_characterController.Actions.InFloor.Movement.canceled += StopMovement; 
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
    }

    public override void Exit()
    {
        base.Exit();
        m_characterController.Actions.InFloor.Jump.performed -= Jump;
        m_characterController.Actions.InFloor.Hit.performed -= Hit;
        m_characterController.Actions.InFloor.Shield.performed -= Shield;
        m_characterController.Actions.InFloor.Combo.performed -= Combo;
        //m_characterController.Actions.InFloor.Movement.performed -= Movement;
        //m_characterController.Actions.InFloor.Movement.canceled -= StopMovement;
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();

        if (m_characterController.Actions.InFloor.Movement.ReadValue<float>() != 0)
            m_FSM.ChangeState<WalkState>();

    }

    private void Jump(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<JumpState>();
    }
    public void Hit(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<HitState>();

    }
    private void Shield(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<ShieldState>();
    }
    public void Combo(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<DownHitState>();

    }

}