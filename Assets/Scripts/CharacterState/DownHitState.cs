using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;
using Unity.Burst.Intrinsics;
using UnityEngine.InputSystem;

public class DownHitState : State
{
    string m_Animation;
    float m_ComboTime;
    float m_StateDelta;
    float m_StateLength;
    CharacterController m_CharacterController;
    public DownHitState(FSM fsm, string animation, float comboTime, float stateLength)
        : base(fsm)
    {
        m_Animation = animation;
        m_ComboTime = comboTime;
        m_StateLength = stateLength;
        m_CharacterController = fsm.Owner.GetComponent<CharacterController>();
        
    }

    public override void Init()
    {
        base.Init();
        m_CharacterController.ImpulseInHit = 0.2f;
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        m_StateDelta = 0;
        m_CharacterController.Actions.InFloor.Combo.performed += Combo;
        m_CharacterController.Damage = 1;
    }

    public override void Update()
    {
        base.Update();
        m_StateDelta += Time.deltaTime;

        

        if(m_StateDelta >= m_StateLength)
        {
            m_FSM.ChangeState<IdleState>();
            return;
        }
    }
    public override void Exit()
    {
        base.Exit();
        m_CharacterController.Actions.InFloor.Combo.performed -= Combo;
    }
    public void Combo(InputAction.CallbackContext context)
    {
        if (m_StateDelta >= m_ComboTime)
        {
            m_FSM.ChangeState<KickState>();
        }
    }
}
