using UnityEngine;
using FiniteStateMachine;
using System;
using UnityEngine.InputSystem;

public class JumpState : State
{
    float m_JumpForce;
    string m_movementAnimation;
    Rigidbody2D m_Rigidbody;
    CharacterController m_CharacterController;
    public JumpState(FSM fSM, string movementAnimation, float jumpForce)
        : base(fSM)
    {
        m_movementAnimation = movementAnimation;
        m_JumpForce = jumpForce;
        m_Rigidbody = fSM.Owner.GetComponent<Rigidbody2D>();
        m_CharacterController = fSM.Owner.GetComponent<CharacterController>();
    }

    public override void Init()
    {
        base.Init();
        m_Rigidbody.AddForce(Vector2.up * m_JumpForce, ForceMode2D.Impulse);
        m_FSM.Owner.GetComponent<Animator>().Play(m_movementAnimation);
        m_CharacterController.Actions.InFloor.Hit.performed += Hit;

    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_Rigidbody.velocity.y < 0 && m_CharacterController.CheckGround().collider != null)
        {
            if (m_CharacterController.Actions.InFloor.Movement.ReadValue<float>() != 0)
                m_FSM.ChangeState<WalkState>();
            else
                m_FSM.ChangeState<IdleState>();
        }
    }
    public override void Exit()
    {
        base.Exit();
        m_CharacterController.Actions.InFloor.Hit.performed -= Hit;
    }
    public void Hit(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<HitState>();

    }
}

