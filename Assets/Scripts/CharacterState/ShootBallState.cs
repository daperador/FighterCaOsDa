using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;

public class ShootBallState : State
{
    string m_Animation;
    float m_StateLength;
    float m_StateDelta;
    CharacterController m_CharacterController;
    energiaPool m_energyPool;
    Vector3 m_otherPlayerPosition;
    public ShootBallState(FSM fsm, string animation, float stateLength, energiaPool energyPool)
        : base(fsm)
    {
        m_Animation = animation;
        m_StateLength = stateLength;
        m_CharacterController = fsm.Owner.GetComponent<CharacterController>();
        m_energyPool = energyPool.GetComponent<energiaPool>();
    }

    public override void Init()
    {
        base.Init();
        m_CharacterController.ImpulseInHit = 1f;
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        m_StateDelta = 0;
        m_CharacterController.Damage = 3;

        GameObject ball;
        if (m_CharacterController.pSelect == CharacterController.PlayerSelect.PlayerBlue)
        {
            ball = m_energyPool.InstanceBolaAzul();
        }
        else
        {
            ball = m_energyPool.InstanceBolaRoja();
        }
        ball.transform.position = m_CharacterController.transform.position;
        Vector2 ballRoute = new Vector2((m_CharacterController.ImpulseHitFaceScale().x / 8), 0);
        ball.GetComponent<Rigidbody2D>().velocity = ballRoute * 4f;
    }

    public override void Update()
    {
        base.Update();
        m_StateDelta += Time.deltaTime;
        m_otherPlayerPosition = m_CharacterController.jugadorContrari.transform.position;
        if (m_StateDelta >= m_StateLength)
        {
            m_FSM.ChangeState<IdleState>();
            return;
        }
    }
}
