using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using UnityEngine.UIElements;
using UnityEditor.Rendering.LookDev;
using UnityEngine.InputSystem;

public class WalkState : State
{
    float m_velocity;
    CharacterController m_CharacterController;
    Rigidbody2D m_Rigidbody; 
    string m_movementAnimation;
    public WalkState(FSM fSM, string animation, float velocity)
        : base(fSM)
    {
        m_velocity = velocity;
        m_Rigidbody = fSM.Owner.GetComponent<Rigidbody2D>();
        m_CharacterController = fSM.Owner.GetComponent<CharacterController>();
        m_movementAnimation=animation;
    }

    public override void Init()
    {
        base.Init();
        m_FSM.Owner.GetComponent<Animator>().Play(m_movementAnimation);
        m_CharacterController.Actions.InFloor.Jump.performed += Jump;
        m_CharacterController.Actions.InFloor.Hit.performed += Hit;

    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        float horizontalAxis = m_CharacterController.Actions.InFloor.Movement.ReadValue<float>();
        if (horizontalAxis != 0f)
        {
            if (horizontalAxis > 0)
            {
                m_Rigidbody.velocity = new Vector2(m_velocity, m_Rigidbody.velocity.y);
                m_FSM.Owner.transform.localScale = m_CharacterController.FaceScale(false);
            }
            else
            {
                m_Rigidbody.velocity = new Vector2(-m_velocity, m_Rigidbody.velocity.y);
                m_FSM.Owner.transform.localScale = m_CharacterController.FaceScale(true);
            }
        }
        else
        {
            m_FSM.ChangeState<IdleState>();
        }

    }
    public override void Exit()
    {
        base.Exit();
        m_CharacterController.Actions.InFloor.Jump.performed -= Jump;
        m_CharacterController.Actions.InFloor.Hit.performed -= Hit;
    }
    private void Jump(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<JumpState>();
    }
    public void Hit(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<HitState>();

    }
}
