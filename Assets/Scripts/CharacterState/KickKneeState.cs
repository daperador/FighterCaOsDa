using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using UnityEngine.InputSystem;
using System;

public class KickKneeState : State
{
    string m_Animation;
    float m_ComboTime;
    float m_StateLength;
    float m_StateDelta;
    ScriptableCards m_cards;
    CharacterController m_CharacterController;

    public KickKneeState(FSM fsm, string animation, float comboTime, float stateLength, ScriptableCards cards)
        : base(fsm)
    {
        m_Animation = animation;
        m_ComboTime = comboTime;
        m_StateLength = stateLength;
        m_CharacterController = fsm.Owner.GetComponent<CharacterController>();
        m_cards = cards;
        
    }

    public override void Init()
    {
        base.Init();
        m_CharacterController.Actions.InFloor.Combo.performed += Combo;
        m_CharacterController.ImpulseInHit = 0.2f;
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        m_StateDelta = 0;
        m_CharacterController.Damage = 0;

    }
    public override void Exit()
    {
        base.Exit();
        m_CharacterController.Actions.InFloor.Combo.performed -= Combo;
    }
    public override void Update()
    {
        base.Update();
        m_StateDelta += Time.deltaTime;

        if (m_StateDelta >= m_StateLength)
        {
            m_FSM.ChangeState<IdleState>();
            return;
        }
    }
    public void Combo(InputAction.CallbackContext context)
    {

        if (m_StateDelta >= m_ComboTime)
        {
            if (m_cards.bolaDisparo>0)
            {
                m_FSM.ChangeState<ShootBallState>();
                m_cards.bolaDisparo--;
            }
            else
            {
                m_FSM.ChangeState<IdleState>();
                return;
            }
            
        }


    }

}
