using FiniteStateMachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.DefaultInputActions;

public class CharacterController : MonoBehaviour
{
    private Rigidbody2D rb;
    private SpriteRenderer spriteRenderer;
    //private bool m_isJumping = false;
    //private Animator m_animator;
    private FSM m_FSM;
    private int m_damage = 0;
    private int m_health;
    private float m_impulse;
    private bool m_inmune;
    public bool Inmune { get { return m_inmune; } set { m_inmune = value; } }
    public int Health { get { return m_health; } set { m_health = value; } }
    public int Damage { get { return m_damage; } set { m_damage = value; } }
    public float ImpulseInHit { get { return m_impulse; } set { m_impulse = value; } }

    //private PlayerInput m_playerInput;

    public enum PlayerSelect { PlayerBlue, PlayerRed };
    [SerializeField] private PlayerSelect m_PlayerSelect;
    public PlayerSelect pSelect { get { return m_PlayerSelect; } private set{} }

    private PlayerController m_playerController;
    public PlayerController Actions => m_playerController;
    [SerializeField] [Range(1, 500)] private float potenciaSalto;
    [SerializeField] private float m_velocity = 1f;
    [SerializeField] LayerMask m_LayerMask;
    [SerializeField] private ScriptableInteger m_healthScriptable;
    [SerializeField] private GameEvent m_damagePlayer;
    [SerializeField] private ScriptableCards m_cardScriptable;
    [SerializeField] private energiaPool m_pool;
    [SerializeField] private GameObject m_jugadorContrari;
    public GameObject jugadorContrari { get { return m_jugadorContrari; } set { m_jugadorContrari = value; } }
    public energiaPool Pool { get { return m_pool; } set { m_pool = value; } }
    [SerializeField] private GameEvent OnPlayerDead;
    void Awake()
    {

        m_playerController = new PlayerController();
        switch (m_PlayerSelect)
         {
             case PlayerSelect.PlayerBlue:
                 m_playerController.bindingMask = new InputBinding { groups = "PlayerBlue" };
                 break;
             case PlayerSelect.PlayerRed:
                 m_playerController.bindingMask = new InputBinding { groups = "PlayerRed" };
                 break;
         }

        m_FSM = new FSM(gameObject);
        m_FSM.AddState(new IdleState(m_FSM, "Idle"));
        m_FSM.AddState(new WalkState(m_FSM, "Walk", m_velocity));
        m_FSM.AddState(new JumpState(m_FSM, "Jump", potenciaSalto));
        m_FSM.AddState(new ShieldState(m_FSM, "Shield", 2f, m_cardScriptable));
        m_FSM.AddState(new HitState(m_FSM, "Hit", 0.7f));
        m_FSM.AddState(new DownHitState(m_FSM, "DownHit", 0.8f, 1.1f));
        m_FSM.AddState(new KickState(m_FSM, "Kick", 0.4f, 1f));
        m_FSM.AddState(new KickKneeState(m_FSM, "KickKnee", 0.5f, 1.5f, m_cardScriptable));
        m_FSM.AddState(new ShootBallState(m_FSM, "Hit", 0.7f, m_pool));


        m_playerController.InFloor.Enable();
        m_FSM.ChangeState<IdleState>();

        rb = GetComponent<Rigidbody2D>();

    }

    void Start()
    {
        m_health=20;
    }

    void Update()
    {
        m_FSM.Update();
    }
    void FixedUpdate()
    {
        m_FSM.FixedUpdate();

    }

    public RaycastHit2D CheckGround()
    {
        return Physics2D.Raycast(transform.position, -transform.up, GetComponent<BoxCollider2D>().bounds.extents.y * 1.3f, m_LayerMask);
    }

    public Vector3 FaceScale(bool changeX)
    {
        if (!changeX)
        {
            return new Vector3(5, 5, 5);
        }
        else
        {
            return new Vector3(-5, 5, 5);
        }
        
    }
    public Vector3 ImpulseHitFaceScale()
    {
        if (transform.localScale.x == -5)
        {
            return new Vector3(-5, 5, 5);
        }
        else
        {
            return new Vector3(5, 5, 5);
        }

    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        m_FSM.OnCollisionEnter2D(collision);
    }
    void OnCollisionStay2D(Collision2D collision)
    {
        m_FSM.OnCollisionStay2D(collision);
    }
    void OnCollisionExit2D(Collision2D collision)
    {
        m_FSM.OnCollisionExit2D(collision);
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        //m_FSM.OnTriggerEnter2D(collider);
        if (collider.gameObject.tag.Equals("Hitbox"))
        {
            if (!m_inmune)
            {
                int DamageToCharacter = collider.gameObject.GetComponentInParent<CharacterController>().Damage;
                m_healthScriptable.valorActual -= DamageToCharacter;
                m_damagePlayer.Raise();
                float impulse = collider.gameObject.GetComponentInParent<CharacterController>().ImpulseInHit;
                Vector3 ImpulseScale = collider.gameObject.GetComponentInParent<CharacterController>().ImpulseHitFaceScale();
                rb.AddForce(ImpulseScale * impulse, ForceMode2D.Impulse);
                if (m_healthScriptable.valorActual <= 0)
                {
                    OnPlayerDead.Raise();
                }
            }
        }

        if (m_PlayerSelect==PlayerSelect.PlayerBlue)
        {
            if (collider.gameObject.tag.Equals("bolaAzul")){
                collider.gameObject.SetActive(false);
                m_cardScriptable.bolaDisparo++;
            }
            if (collider.gameObject.tag.Equals("inmuneAzul"))
            {
                collider.gameObject.SetActive(false);
                m_cardScriptable.bolaInmunidad++;
            }
            if (collider.gameObject.tag.Equals("pocionAzul"))
            {
                collider.gameObject.SetActive(false);
                m_cardScriptable.bolaCura++;
                if (m_healthScriptable.valorActual < m_healthScriptable.valorTotal)
                {
                    m_healthScriptable.valorActual++;
                    m_damagePlayer.Raise();
                }
                m_cardScriptable.bolaCura--;
            }
            if (collider.gameObject.tag.Equals("energiaRoja"))
            {
                if (!m_inmune)
                {
                    int DamageToCharacter = m_jugadorContrari.GetComponent<CharacterController>().Damage;
                    m_healthScriptable.valorActual -= DamageToCharacter;
                    m_damagePlayer.Raise();
                    collider.gameObject.SetActive(false);

                    if (m_healthScriptable.valorActual <= 0)
                    {
                        OnPlayerDead.Raise();
                    }
                }
                else
                {
                    Vector2 recalculatedBallRoute = new Vector2((ImpulseHitFaceScale().x / 8), -2);
                    collider.gameObject.GetComponent<Rigidbody2D>().velocity = recalculatedBallRoute * 4f;
                }
            }
        }
        if (m_PlayerSelect == PlayerSelect.PlayerRed)
        {
            if (collider.gameObject.tag.Equals("bolaRoja"))
            {
                collider.gameObject.SetActive(false);
                m_cardScriptable.bolaDisparo++;
            }
            if (collider.gameObject.tag.Equals("inmuneRojo"))
            {
                collider.gameObject.SetActive(false);
                m_cardScriptable.bolaInmunidad++;
            }
            if (collider.gameObject.tag.Equals("pocionRoja"))
            {
                collider.gameObject.SetActive(false);
                m_cardScriptable.bolaCura++;
                if (m_healthScriptable.valorActual< m_healthScriptable.valorTotal)
                {
                    m_healthScriptable.valorActual++;
                    m_damagePlayer.Raise();
                }
                m_cardScriptable.bolaCura--;
            }
            if (collider.gameObject.tag.Equals("energiaAzul"))
            {
                if (!m_inmune)
                {
                    int DamageToCharacter = m_jugadorContrari.GetComponent<CharacterController>().Damage;
                    m_healthScriptable.valorActual -= DamageToCharacter;
                    m_damagePlayer.Raise();
                    collider.gameObject.SetActive(false);
                    if (m_healthScriptable.valorActual <= 0)
                    {
                        OnPlayerDead.Raise();
                    }
                }
                else
                {
                    Vector2 recalculatedBallRoute = new Vector2((ImpulseHitFaceScale().x / 8), -2);
                    collider.gameObject.GetComponent<Rigidbody2D>().velocity = recalculatedBallRoute * 4f;
                }
            }
        }
    }
    void OnTriggerStay2D(Collider2D collider)
    {
        m_FSM.OnTriggerStay2D(collider);
    }
    void OnTriggerExit2D(Collider2D collider)
    {
        m_FSM.OnTriggerExit2D(collider);
    }



}
