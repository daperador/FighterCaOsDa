using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{

    [SerializeField] GameObject[] puntsSpawner;
    [SerializeField]private ItemsPool itemsPool;

    private void Start()
    {
        StartCoroutine(spawnItems(puntsSpawner));
    }

    IEnumerator spawnItems(GameObject[] p) {
        yield return new WaitForSeconds(10);
        while (true)
        {
            GameObject itemFighter = itemsPool.InstanceItem();
            itemFighter.transform.position = p[Random.Range(0, p.Length)].transform.position;
            itemFighter.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -2, 0);
            yield return new WaitForSeconds(10);
        }
      
    }

    public void OnAnyPlayerDead()
    {
        SceneManager.LoadScene("GameOver");
    }
}
