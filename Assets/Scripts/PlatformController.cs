using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    SliderJoint2D platform;
    Rigidbody2D rigidBody;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        platform = gameObject.GetComponent<SliderJoint2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (platform.limits.max <= transform.position.y)
        {
            platform.useMotor = false;
        }
        if (rigidBody.velocity.y==0)
        {
            platform.useMotor = true;
        }
    }
}
