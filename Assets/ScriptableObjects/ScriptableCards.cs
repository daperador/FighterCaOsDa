using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Scriptable Cards")]
public class ScriptableCards : ScriptableObject
{
    public int bolaDisparo;
    public int bolaInmunidad;
    public int bolaCura;
}
